library(shiny)
library(ggplot2)
library(drc)

server <- function(session,input,output) {
  
  dataframe<-reactive({
    if (is.null(input$datafile))
      return(NULL)                
    data<-read.csv(input$datafile$datapath, header = FALSE)
    data_start <- which(data[,1] == 'A', arr.ind = TRUE)[1]
    data <- data[data_start:(data_start+7),3:11]
    data
  })

  output$table <- renderTable({
    dataframe()
  })
    
  # output$plot <- renderPlot({
  # concentration <- c(1e-5,1e-4,1e-3,1e-2,1e-1,1,10,100,1000)
  # data <- dataframe()
  # i = 1
  # subset <- cbind((as.numeric(t(data[i,]))), concentration)
  # ms<-drm(subset[,1]~subset[,2],na.action=na.omit,fct=LL.4())
  # plot(ms)
  # })
  # 
  # Insert the right number of plot output objects into the web page
  max_plots <- 8
  output$plots <- renderUI({
    plot_output_list <- lapply(1:max_plots, function(i) {
      plotname <- paste("plot", i, sep="")
      plotOutput(plotname, height = 280, width = 250)
    })
    
    # Convert the list to a tagList - this is necessary for the list of items
    # to display properly.
    do.call(tagList, plot_output_list)
  })
  
  # Call renderPlot for each one. Plots are only actually generated when they
  # are visible on the web page.
  for (i in 1:max_plots) {
    # Need local so that each item gets its own number. Without it, the value
    # of i in the renderPlot() will be the same across all instances, because
    # of when the expression is evaluated.
    local({
      my_i <- i
      plotname <- paste("plot", my_i, sep="")
      
      output[[plotname]] <- renderPlot({
        plot(ms,)
        })
    })
  }

}

